import command.CommandsTest;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({
    CommandsTest.class
})
public class TestSuiteCommands {    
}
