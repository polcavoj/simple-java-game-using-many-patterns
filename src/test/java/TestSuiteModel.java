import model.CannonTest;
import model.GameObjectTest;
import model.ModelInfoTest;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({
    GameObjectTest.class,
    CannonTest.class,
    ModelInfoTest.class
})
public class TestSuiteModel {    
}
