package model;

import cz.fit.miadp.mvcgame.model.Enemy;
import cz.fit.miadp.mvcgame.model.GameObject;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.runner.RunWith;
import org.mockito.Matchers;
import org.mockito.Mockito;
import static org.mockito.Mockito.when;
import org.mockito.runners.MockitoJUnitRunner;


@RunWith(MockitoJUnitRunner.class)
public class GameObjectTest {
    
    GameObject gameObject;
    
    @Before
    public void initial() {
        gameObject = Mockito.mock(GameObject.class);
        when(gameObject.getX()).thenReturn(300);
        when(gameObject.getY()).thenReturn(300);
        Mockito.doCallRealMethod().when(gameObject).collidesWith((GameObject) Matchers.anyObject());
    }
    
    @Test
    public void testCollidesWithPositive(){
        Enemy enemy = new Enemy();
        enemy.setX(305);
        enemy.setY(306);
        
        boolean res = gameObject.collidesWith(enemy);
        assertEquals(true, res);
    }
    
    @Test
    public void testCollidesWithNegative(){
        Enemy enemy = new Enemy();
        enemy.setX(350);
        enemy.setY(306);
        
        boolean res = gameObject.collidesWith(enemy);
        assertEquals(false, res);
    }
    
    @Test
    public void testCollidesWithBoundary(){
        Enemy enemy = new Enemy();
        Enemy enemy2 = new Enemy();
        enemy.setX(320);
        enemy.setY(320);
        enemy2.setX(319);
        enemy2.setY(319);
        
        boolean res = gameObject.collidesWith(enemy);
        boolean res2 = gameObject.collidesWith(enemy2);
        assertEquals(false, res);
        assertEquals(true, res2);
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
}
