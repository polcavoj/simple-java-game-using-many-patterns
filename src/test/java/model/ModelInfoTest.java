package model;

import cz.fit.miadp.mvcgame.model.Cannon;
import cz.fit.miadp.mvcgame.model.Enemy;
import cz.fit.miadp.mvcgame.model.GameModel;
import cz.fit.miadp.mvcgame.model.Missile;
import cz.fit.miadp.mvcgame.model.ModelInfo;
import cz.fit.miadp.mvcgame.model.StrongerEnemy;
import java.util.ArrayList;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class ModelInfoTest {

    @Test
    public void testModelInfo() {
        /* Mocked fields. */
        ArrayList<Enemy> enemies = Mockito.mock(ArrayList.class);
        when(enemies.size()).thenReturn(5);
        ArrayList<StrongerEnemy> strongerEnemies = Mockito.mock(ArrayList.class);
        when(strongerEnemies.size()).thenReturn(1);
        ArrayList<Missile> missiles = Mockito.mock(ArrayList.class);
        when(missiles.size()).thenReturn(3);
        
        /* Mocked cannon. */
        Cannon cannon = Mockito.mock(Cannon.class);
        when(cannon.getY()).thenReturn(320);
        when(cannon.getAngle()).thenReturn(0.8f);
        when(cannon.getVelocity()).thenReturn(11.0f);
        
        /* Setting mocking parameters for model. */
        GameModel model = Mockito.mock(GameModel.class);
        when(model.getScore()).thenReturn(20);
        when(model.getEnemies()).thenReturn(enemies);
        when(model.getStrongerEnemies()).thenReturn(strongerEnemies);
        when(model.getMissiles()).thenReturn(missiles);  
        when(model.getCannon()).thenReturn(cannon);
        
        
        ModelInfo modelInfo = new ModelInfo(model);        
        String res = "Score: 20 Cannon.y: 320 Angle: 0.8 Power: 11.0 Enem.size: 6 Miss.size: 3";

        assertEquals(res, modelInfo.getText());
    }
}
