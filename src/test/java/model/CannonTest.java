package model;

import cz.fit.miadp.mvcgame.abstractFactory.DefaultGameObjectFactory;
import cz.fit.miadp.mvcgame.config.GameConfig;
import cz.fit.miadp.mvcgame.model.Cannon;
import cz.fit.miadp.mvcgame.model.GameObject;
import cz.fit.miadp.mvcgame.model.Missile;
import cz.fit.miadp.mvcgame.strategy.SimpleMovementStrategy;
import java.util.List;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class CannonTest {

    
    private static final double DELTA = 1E-6;
    
    private DefaultGameObjectFactory factory;

    
    @Before
    public void initial() {
        factory = Mockito.mock(DefaultGameObjectFactory.class);
    }

    @Test
    public void testCannonAim(){
        Cannon cannon = new Cannon(factory);
        cannon.setAngle(1.4f);
        
        
        cannon.aimDown();
        assertEquals(1.4f + GameConfig.ANGLE_STEP, cannon.getAngle(), DELTA);
        
        cannon.aimDown();
        assertEquals(1.5f + GameConfig.ANGLE_STEP, cannon.getAngle(), DELTA);
        
        cannon.aimUp();
        assertEquals(1.6f - GameConfig.ANGLE_STEP, cannon.getAngle(), DELTA);
    }
    
    @Test
    public void testCannonPower(){
        Cannon cannon = new Cannon(factory);        
        
        cannon.incPower();
        cannon.incPower();
        assertEquals(10.0f + 2*(GameConfig.VELOCITY_STEP), cannon.getVelocity(), DELTA);
        
        cannon.decPower();
        assertEquals(12.0f - GameConfig.VELOCITY_STEP, cannon.getVelocity(), DELTA);
    }
    
    @Test
    public void testShootingMode(){
        Missile missile = new Missile(1, 1, 1.6f, 1.5f, new SimpleMovementStrategy());
        Mockito.when(factory.createMissile()).thenReturn(missile);
        
        Cannon cannon = new Cannon(factory);   
        List<Missile> shootBatch = cannon.shoot();
        
        assertNotNull(shootBatch);
        assertEquals(1, shootBatch.size());
        
        
        cannon.toggleShootingMode();
        shootBatch = cannon.shoot();
        
        assertNotNull(shootBatch);
        assertEquals(2, shootBatch.size());
    }
}
