package command;

import cz.fit.miadp.mvcgame.command.*;
import cz.fit.miadp.mvcgame.controller.GameController;
import cz.fit.miadp.mvcgame.model.GameModel;
import java.awt.event.KeyEvent;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.mockito.Matchers.isA;
import org.mockito.Mockito;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class CommandsTest {
   
    private KeyEvent keyEvent;
    private GameModel gameModel;
    private GameController gameController;
    
    @Before
    public void initial() {
        keyEvent = Mockito.mock(KeyEvent.class);
        gameModel = Mockito.mock(GameModel.class);
        gameController = new GameController();
        gameController.setModel(gameModel);
    }
    
    @Test
    public void testAimCannonDownCommand(){
        when(keyEvent.getKeyCode()).thenReturn(KeyEvent.VK_S);
        gameController.onKeyPress(keyEvent);
        
        verify(gameModel).registerCmd(isA(AimCannonDownCommand.class));
    }
    
    @Test
    public void testAimCannonUpCommand(){
        when(keyEvent.getKeyCode()).thenReturn(KeyEvent.VK_W);
        gameController.onKeyPress(keyEvent);
        
        verify(gameModel).registerCmd(isA(AimCannonUpCommand.class));
    }
    
    @Test
    public void testCannonShootCommand(){
        when(keyEvent.getKeyCode()).thenReturn(KeyEvent.VK_SPACE);
        gameController.onKeyPress(keyEvent);
        
        verify(gameModel).registerCmd(isA(CannonShootCommand.class));
    }
    
    @Test
    public void testDecCannonPowerCommand(){
        when(keyEvent.getKeyCode()).thenReturn(KeyEvent.VK_Q);
        gameController.onKeyPress(keyEvent);
        
        verify(gameModel).registerCmd(isA(DecCannonPowerCommand.class));
    }
    
    @Test
    public void testIncCannonPowerCommand(){
        when(keyEvent.getKeyCode()).thenReturn(KeyEvent.VK_E);
        gameController.onKeyPress(keyEvent);
        
        verify(gameModel).registerCmd(isA(IncCannonPowerCommand.class));
    }
    
    @Test
    public void testMoveCannonDownCommand(){
        when(keyEvent.getKeyCode()).thenReturn(KeyEvent.VK_DOWN);
        gameController.onKeyPress(keyEvent);
        
        verify(gameModel).registerCmd(isA(MoveCannonDownCommand.class));
    }
    
    @Test
    public void testMoveCannonUpCommand(){
        when(keyEvent.getKeyCode()).thenReturn(KeyEvent.VK_UP);
        gameController.onKeyPress(keyEvent);
        
        verify(gameModel).registerCmd(isA(MoveCannonUpCommand.class));
    }
    
    @Test
    public void testSwitchMovementStrategyCommand(){
        when(keyEvent.getKeyCode()).thenReturn(KeyEvent.VK_C);
        gameController.onKeyPress(keyEvent);
        
        verify(gameModel).registerCmd(isA(SwitchMovementStrategyCommand.class));
    }
    
    @Test
    public void testToogleShootingModeommand(){
        when(keyEvent.getKeyCode()).thenReturn(KeyEvent.VK_X);
        gameController.onKeyPress(keyEvent);
        
        verify(gameModel).registerCmd(isA(ToogleShootingModeCommand.class));
    }
    
}
