package cz.fit.miadp.mvcgame.abstractFactory;

import java.util.Random;

import cz.fit.miadp.mvcgame.model.Cannon;
import cz.fit.miadp.mvcgame.model.Collision;
import cz.fit.miadp.mvcgame.model.Enemy;
import cz.fit.miadp.mvcgame.model.Explosion;
import cz.fit.miadp.mvcgame.model.GameModel;
import cz.fit.miadp.mvcgame.model.Missile;
import cz.fit.miadp.mvcgame.model.ModelInfo;
import cz.fit.miadp.mvcgame.model.StrongerEnemy;

public class DefaultGameObjectFactory implements IGameObjectsFactory
{
    protected Random random = new Random();
    protected GameModel model;

    public DefaultGameObjectFactory(GameModel model){
        this.model = model;
    }
    
    public Cannon createCannon(){
        return new Cannon(this);
    }

    public Enemy createEnemy(){
        int posX = (2*this.model.getCannon().getX()) + random.nextInt(this.model.getMaxX()-(2*this.model.getCannon().getX()));
        int posY = random.nextInt(this.model.getMaxY());

        Enemy anEnemy = new Enemy();
        anEnemy.setX(posX);
        anEnemy.setY(posY);
        return anEnemy;
    }
    
    public StrongerEnemy createStrongerEnemy(){
        int posX = (2*this.model.getCannon().getX()) + random.nextInt(this.model.getMaxX()-(2*this.model.getCannon().getX()));
        int posY = random.nextInt(this.model.getMaxY());

        StrongerEnemy strongerEnemy = new StrongerEnemy(posX, posY);
        strongerEnemy.setHealth(3);
        return strongerEnemy;
    }

    public Missile createMissile(){
        return new Missile(
            this.model.getCannon().getX()
            ,this.model.getCannon().getY()
            ,this.model.getCannon().getVelocity()
            ,this.model.getCannon().getAngle()
            ,this.model.getActiveMovementStrategy()
        );
    }

    public Collision createCollision(int x, int y){
        Collision col = new Collision();
        col.setX(x);
        col.setY(y);
        return col;
    }
    
    public Explosion createExplosion(int x, int y){
        Explosion exp = new Explosion();
        exp.setX(x);
        exp.setY(y);
        return exp;
    }
    
    public ModelInfo createModelInfo(){
        return new ModelInfo(this.model);
    }
}