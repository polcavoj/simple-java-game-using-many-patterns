package cz.fit.miadp.mvcgame.config;

public class GameConfig 
{
    public static final int MAX_X = 1000;
    public static final int MAX_Y = 700;
    public static final int MAX_ENEMIES = 10;
    public static final int MAX_STRONGER_ENEMIES = 3;

    public static final int MOVE_STEP = 10;
    public static final float GRAVITY_STEP = 30.0f;
    public static final float VELOCITY_STEP = 1.0f;
    public static final float ANGLE_STEP = 0.1f;
    public static final int ENEMY_STEP = 4;
    
    public static final int TIME_TICK = 20;
    public static final int COLLIDE_FACTOR = 20;
    public static final int COLLISION_LIFETIME = 2000;
    public static final int EXPLOSION_LIFETIME = 250;

    public static final int INIT_SCORE = 0;
    public static final float INIT_GRAVITY = 10.0f;
    public static final float INIT_VELOCITY = 10.0f;
    public static final float INIT_ANGLE = 0.0f;
    
    public static final int MIN_DISTANCE = 50;
    public static final int MIN_POWER = 1;
    public static final int MAX_POWER = 30;
    public static final float MIN_ANGLE = 1.5f;
    public static final float MAX_ANGLE = -1.5f;
    public static final int MAX_RANGE_OF_ENEMY = 100;
}