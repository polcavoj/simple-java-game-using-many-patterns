package cz.fit.miadp.mvcgame.controller;

import cz.fit.miadp.mvcgame.command.*;
import java.awt.event.KeyEvent;
import cz.fit.miadp.mvcgame.proxy.IGameModel;

public class GameController
{
    private IGameModel model;
    

    public GameController()
    {
        
    }

    public IGameModel getModel()
    {
        return this.model;
    }

    public void setModel(IGameModel model)
    {
        this.model = model;
    }

    
    public void onKeyPress(KeyEvent evt)
    {
        if(!(this.model instanceof IGameModel)) return;
        
        System.out.println("key pressed: " 
            + evt.getKeyChar()
            + "(code: "+ evt.getKeyCode() +")");

        switch(evt.getKeyCode())
        {
            case KeyEvent.VK_UP:
                this.model.registerCmd( new MoveCannonUpCommand(this.model));
                break;
            case KeyEvent.VK_DOWN:
                this.model.registerCmd( new MoveCannonDownCommand(this.model));
                break;
            case KeyEvent.VK_SPACE:
                this.model.registerCmd( new CannonShootCommand(this.model));
                break;
            case KeyEvent.VK_W:
                this.model.registerCmd( new AimCannonUpCommand(this.model));
                break;
            case KeyEvent.VK_S:
                this.model.registerCmd( new AimCannonDownCommand(this.model));
                break;
            case KeyEvent.VK_Z:
                this.model.undoLastCmd();
                break;
            case KeyEvent.VK_E:
                this.model.registerCmd( new IncCannonPowerCommand(this.model));
                break;    
            case KeyEvent.VK_Q:
                this.model.registerCmd( new DecCannonPowerCommand(this.model));
                break;
            case KeyEvent.VK_C:
                this.model.registerCmd( new SwitchMovementStrategyCommand(this.model));
                break;    
            case KeyEvent.VK_X:
                this.model.registerCmd( new ToogleShootingModeCommand(this.model));
                break;
            default:
                //nothing
        }
    
    }
}