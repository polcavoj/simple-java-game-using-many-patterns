package cz.fit.miadp.mvcgame.command;

import cz.fit.miadp.mvcgame.proxy.IGameModel;

public class IncCannonPowerCommand extends AbsGameCommand {

    public IncCannonPowerCommand(IGameModel subject) {
        super(subject);
    }

    @Override
    public void execute() {
        subject.incCannonPower();
    }
    
    
}
