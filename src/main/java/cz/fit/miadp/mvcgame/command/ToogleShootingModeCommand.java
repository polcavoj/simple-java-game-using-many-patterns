package cz.fit.miadp.mvcgame.command;

import cz.fit.miadp.mvcgame.proxy.IGameModel;

public class ToogleShootingModeCommand extends AbsGameCommand {

    public ToogleShootingModeCommand(IGameModel subject) {
        super(subject);
    }

    @Override
    public void execute() {
        subject.toggleShootingMode();
    }
    
}
