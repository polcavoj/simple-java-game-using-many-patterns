package cz.fit.miadp.mvcgame.state;

import cz.fit.miadp.mvcgame.model.Cannon;

public interface IShootingMode
{
    void shoot(Cannon cannon);
    void nextMode(Cannon cannon);
}