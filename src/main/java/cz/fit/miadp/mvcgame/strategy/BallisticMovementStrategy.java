package cz.fit.miadp.mvcgame.strategy;

import cz.fit.miadp.mvcgame.config.GameConfig;

public class BallisticMovementStrategy implements IMovementStrategy {

    @Override
    public int nextPosX(int initX, Float initVelocity, Float initAngle, long lifetime) {
        return (int)(initX + (initVelocity * Math.cos(initAngle) * lifetime));
    }

    @Override
    public int nextPosY(int initY, Float initVelocity, Float initAngle, long lifetime) {
        double g = GameConfig.GRAVITY_STEP;
        double t = (double) lifetime / 1000;
        
        return (int)(Math.round(initY + initVelocity * t * Math.sin(Math.toRadians(initAngle)) + g * Math.pow(t, 2) / 2));
    }
    
}