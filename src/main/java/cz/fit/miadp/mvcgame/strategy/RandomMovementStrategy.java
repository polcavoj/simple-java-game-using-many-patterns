package cz.fit.miadp.mvcgame.strategy;

import java.util.Random;

public class RandomMovementStrategy implements IMovementStrategy
{
    Random random = new Random();

    @Override
    public int nextPosX(int initX, Float initVelocity, Float initAngle, long lifetime) {
        return (random.nextInt(50)-25) + (int)(initX + (initVelocity * lifetime * Math.cos(initAngle) ));
    }

    @Override
    public int nextPosY(int initY, Float initVelocity, Float initAngle, long lifetime) {
        return (random.nextInt(50)-25) + (int)(initY + (initVelocity * lifetime * Math.sin(initAngle) ));
    }


}