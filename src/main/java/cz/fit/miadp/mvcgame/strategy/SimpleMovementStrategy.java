package cz.fit.miadp.mvcgame.strategy;

public class SimpleMovementStrategy implements IMovementStrategy
{

    @Override
    public int nextPosX(int initX, Float initVelocity, Float initAngle, long lifetime) {
        return (int)(initX + (initVelocity * lifetime * Math.cos(initAngle) ));
    }

    @Override
    public int nextPosY(int initY, Float initVelocity, Float initAngle, long lifetime) {
        return (int)(initY + (initVelocity * lifetime * Math.sin(initAngle) ));
    }


}