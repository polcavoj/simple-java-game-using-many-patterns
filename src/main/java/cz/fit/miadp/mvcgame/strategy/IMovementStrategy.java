package cz.fit.miadp.mvcgame.strategy;

public interface IMovementStrategy
{
    int nextPosX(int initX, Float initVelocity, Float initAngle, long lifetime);
    int nextPosY(int initY, Float initVelocity, Float initAngle, long lifetime);
}