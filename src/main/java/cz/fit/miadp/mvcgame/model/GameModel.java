package cz.fit.miadp.mvcgame.model;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import cz.fit.miadp.mvcgame.abstractFactory.DefaultGameObjectFactory;
import cz.fit.miadp.mvcgame.abstractFactory.IGameObjectsFactory;
import cz.fit.miadp.mvcgame.command.AbsGameCommand;
import cz.fit.miadp.mvcgame.config.GameConfig;
import cz.fit.miadp.mvcgame.observer.IObservable;
import cz.fit.miadp.mvcgame.observer.IObserver;
import cz.fit.miadp.mvcgame.proxy.IGameModel;
import cz.fit.miadp.mvcgame.strategy.BallisticMovementStrategy;
import cz.fit.miadp.mvcgame.strategy.IMovementStrategy;
import cz.fit.miadp.mvcgame.strategy.RandomMovementStrategy;
import cz.fit.miadp.mvcgame.strategy.SimpleMovementStrategy;
import java.util.Queue;
import java.util.Set;
import java.util.Stack;
import java.util.concurrent.LinkedBlockingQueue;

public class GameModel implements IObservable, IGameModel
{
    private int score = GameConfig.INIT_SCORE;
    private Cannon cannon;
    private ArrayList<Enemy> enemies = new ArrayList<Enemy>();
    private ArrayList<StrongerEnemy> strongerEnemies = new ArrayList<StrongerEnemy>();
    private ArrayList<Missile> missiles = new ArrayList<Missile>();
    private ArrayList<Collision> collisions = new ArrayList<Collision>();
    private ArrayList<Explosion> explosions = new ArrayList<Explosion>();
    
    private IGameObjectsFactory goFact = new DefaultGameObjectFactory(this);
    private Timer timer;
    private ArrayList<IObserver> myObservers = new ArrayList<IObserver>();
    private Queue<AbsGameCommand> unexecutedCmds = new LinkedBlockingQueue<AbsGameCommand>();
    private Stack<AbsGameCommand> executedCmds = new Stack<AbsGameCommand>();
     
    private final List<IMovementStrategy> movementStrategies = new ArrayList<IMovementStrategy>();
    private int activeMovementStrategyIndex = 0;

    public GameModel()
    {
        movementStrategies.add( new SimpleMovementStrategy() );
        movementStrategies.add( new RandomMovementStrategy() );
        movementStrategies.add( new BallisticMovementStrategy() );
        

        this.initGameObjects();    
        this.initTimer();    
    }

    public void initTimer() {
        this.timer = new Timer();
        this.timer.schedule(new TimerTask(){
            @Override
            public void run() {
                moveGameObjects(); 
                executeCmds();
            }
        }, 0, GameConfig.TIME_TICK);
    }
    
    private void executeCmds() {
        while(!this.unexecutedCmds.isEmpty() ){
            AbsGameCommand cmd = this.unexecutedCmds.poll();
            cmd.extExecute();
            this.executedCmds.push(cmd);
        }
    }
    
    @Override
    public void undoLastCmd(){
        if( !this.executedCmds.isEmpty() ){
            AbsGameCommand cmd = this.executedCmds.pop();
            cmd.unexecute();
        }
    }
    
    @Override
    public void registerCmd(AbsGameCommand cmd){
        this.unexecutedCmds.add(cmd);
    }

    protected void moveMissiles() {
        for(Missile m : this.missiles)
            m.move();
    
    }
    
    protected void moveEnemies(){
        for(StrongerEnemy e : this.strongerEnemies )
            e.move();
    }

    protected void moveGameObjects() {
        this.moveMissiles();
        
        this.moveEnemies();

        this.removeUnvisibleMissiles();

        this.handleCollisions();
        
        if( this.getEnemies().size() < ( GameConfig.MAX_ENEMIES / 2 ) ){
            this.generateNewEnemies();
        }
        
        if( this.getStrongerEnemies().isEmpty() ){
            this.generateNewStrongerEnemies();
        }

        this.notifyMyObservers();
    }
    
    private void generateNewEnemies() {
        for(int i=this.getEnemies().size(); i < GameConfig.MAX_ENEMIES; i++)
            enemies.add( this.goFact.createEnemy() );
    }
    
    private void generateNewStrongerEnemies() {
        for(int i=this.getStrongerEnemies().size(); i < GameConfig.MAX_STRONGER_ENEMIES; i++)
            strongerEnemies.add( this.goFact.createStrongerEnemy() );
    }

    private void handleCollisions() {
        Set<Enemy> enemiesToRemove = new HashSet<Enemy>();
        Set<StrongerEnemy> strongerEnemiesToRemove = new HashSet<StrongerEnemy>();
        Set<Missile> missilesToRemove = new HashSet<Missile>();
        Set<Collision> collisionsToRemove = new HashSet<Collision>();
        Set<Explosion> explosionsToRemove = new HashSet<Explosion>();

        for(Missile m : this.missiles){
            
            for(Enemy e : this.enemies){
                
                if(m.collidesWith(e)){
                    this.score++;
                    
                    enemiesToRemove.add(e);
                    missilesToRemove.add(m);
                    
                    this.explosions.add( goFact.createExplosion(e.getX(), e.getY()));
                    this.collisions.add( goFact.createCollision(e.getX(), e.getY()));
                }
            }
            
            for(StrongerEnemy e : this.strongerEnemies){
                
                if(m.collidesWith(e)){
                    if( e.getHealth() <= 1 ){
                        this.score+=2;
                        strongerEnemiesToRemove.add(e);
                        this.collisions.add( goFact.createCollision(e.getX(), e.getY()));
                    }
                    
                    e.setHealth(e.getHealth() - 1);
                    missilesToRemove.add(m);
                    this.explosions.add( goFact.createExplosion(e.getX(), e.getY()));
                }
            }
        }

        for( Collision c : this.collisions ){
            if(c.getLifetime() > GameConfig.COLLISION_LIFETIME )
                collisionsToRemove.add(c);
        }
        
        for( Explosion exp : this.explosions ){
            if(exp.getLifetime() > GameConfig.EXPLOSION_LIFETIME )
                explosionsToRemove.add(exp);
        }

        for( Enemy e : enemiesToRemove ){
            this.enemies.remove(e);
        }
        
        for( Missile m : missilesToRemove ){
            this.missiles.remove(m);
        }
        
        for( Collision c : collisionsToRemove ){
            this.collisions.remove(c);
        }
        
        for( Explosion exp : explosionsToRemove ){
            this.explosions.remove(exp);
        }
        
        for( StrongerEnemy e : strongerEnemiesToRemove ){
            this.strongerEnemies.remove(e);
        }
    }

    protected void removeUnvisibleMissiles() {
        HashSet<Missile> toRemove = new HashSet<Missile>();
        for(Missile m : this.missiles)
        {
            if( m.getX() > this.getMaxX() || m.getX() < 0 )
                toRemove.add(m);

            if( m.getY() > this.getMaxY() || m.getY() < 0 )
                toRemove.add(m);
        }

        for(Missile m : toRemove)
            this.missiles.remove(m);
    }

    public void initGameObjects() {
        this.cannon = this.goFact.createCannon();
       
        enemies.clear();
        for(int i=0; i < GameConfig.MAX_ENEMIES; i++)
            enemies.add( this.goFact.createEnemy() );
        
        strongerEnemies.clear();
        for(int i=0; i < GameConfig.MAX_STRONGER_ENEMIES; i++)
            strongerEnemies.add( this.goFact.createStrongerEnemy() );

        missiles.clear();
    }

    @Override
    public int getScore()
    {
        return this.score;
    }

    @Override
    public int getMaxX()
    {
        return GameConfig.MAX_X;
    }

    @Override
    public int getMaxY()
    {
        return GameConfig.MAX_Y;
    }

    @Override
    public Cannon getCannon()
    {
        return this.cannon;
    }

    @Override
    public ModelInfo getInfo()
    {
        return this.goFact.createModelInfo();
    }

    @Override
    public ArrayList<Enemy> getEnemies()
    {
        return this.enemies;
    }
    
    @Override
    public ArrayList<StrongerEnemy> getStrongerEnemies()
    {
        return this.strongerEnemies;
    }

    @Override
    public void moveCannonUp()
    {
        int y = this.cannon.getY();
        if( y > GameConfig.MIN_DISTANCE ){
            y -= GameConfig.MOVE_STEP;
            this.cannon.setY(y);
        }

        this.notifyMyObservers();
    }

    @Override
    public void moveCannonDown()
    {
        int y = this.cannon.getY();
        if( GameConfig.MIN_DISTANCE < (GameConfig.MAX_Y - y ) ){
            y += GameConfig.MOVE_STEP;
            this.cannon.setY(y);
        }

        this.notifyMyObservers();
    }

    @Override
    public void cannonShoot()
    {
        this.missiles.addAll( this.cannon.shoot() );

        this.notifyMyObservers();
    }

    @Override
    public void attachObserver(IObserver observer)
    {
        if(!this.myObservers.contains(observer))
            this.myObservers.add(observer);
    }

    @Override
    public void deattachObserver(IObserver observer)
    {
        this.myObservers.remove(observer);
    }

    @Override
    public void notifyMyObservers()
    {
        for (IObserver observer : this.myObservers) {
            observer.update();
        }
    }

    @Override
    public ArrayList<Missile> getMissiles() {
        return this.missiles;
    }

    @Override
    public void aimCannonUp() {
        if( this.cannon.getAngle() > GameConfig.MAX_ANGLE ){
            this.cannon.aimUp();
        }
        this.notifyMyObservers();
    }

    @Override
    public void aimCannonDown() {
        if( this.cannon.getAngle() < GameConfig.MIN_ANGLE ){
            this.cannon.aimDown();
        }
        this.notifyMyObservers();
    }

    @Override
    public void incCannonPower() {
        if( this.cannon.getVelocity() < GameConfig.MAX_POWER ){
            this.cannon.incPower();
        }
        this.notifyMyObservers();
    }

    @Override
    public void decCannonPower() {
        if( this.cannon.getVelocity() > GameConfig.MIN_POWER ){
            this.cannon.decPower();
        }
        this.notifyMyObservers();
    }
    
    @Override
    public void switchMovementStrategy()
    {
        activeMovementStrategyIndex = (activeMovementStrategyIndex + 1) % movementStrategies.size();
        this.notifyMyObservers();
    }

    @Override
    public ArrayList<GameObject> getGameObjects() {
        ArrayList<GameObject> go = new ArrayList<GameObject>();

        go.addAll(this.missiles);
        go.addAll(this.enemies);
        go.addAll(this.strongerEnemies);
        go.addAll(this.collisions);
        go.addAll(this.explosions);
        go.add(this.cannon);
        go.add(this.getInfo());

        return go;
    }

    @Override
    public IMovementStrategy getActiveMovementStrategy() {
        return this.movementStrategies.get( activeMovementStrategyIndex );
    }

    @Override
    public void toggleShootingMode() {
        this.cannon.toggleShootingMode();
        notifyMyObservers();
    }

    @Override
    public void setMemento(Object memento) {
        GameMemento memento2 = (GameMemento)memento;
        this.score = memento2.score;
        this.cannon.setY(memento2.cannonPosY);
        this.cannon.setAngle(memento2.cannonAngle);
        this.cannon.setVelocity(memento2.cannonVelocity);
        this.enemies = memento2.enemies;
        this.strongerEnemies = memento2.strongerEnemies;
    }

    @Override
    public Object createMemento() {
        GameMemento memento = new GameMemento();
        memento.score = this.score;
        memento.cannonPosY = this.cannon.getY();
        memento.cannonAngle = this.cannon.getAngle();
        memento.cannonVelocity = this.cannon.getVelocity();
        memento.enemies.clear();
        memento.strongerEnemies.clear();
        
        for(int i = 0; i < this.getEnemies().size(); i++ ){
            memento.enemies.add(enemies.get(i));
        }
        for(int i = 0; i < this.getStrongerEnemies().size(); i++ ){
            memento.strongerEnemies.add(strongerEnemies.get(i));
        }
        
        return memento;
    }
    
    private class GameMemento{
        public int score;
        public int cannonPosY;
        public float cannonAngle;
        public float cannonVelocity;
        public ArrayList<Enemy> enemies = new ArrayList<Enemy>();
        private ArrayList<StrongerEnemy> strongerEnemies = new ArrayList<StrongerEnemy>();
    }

}