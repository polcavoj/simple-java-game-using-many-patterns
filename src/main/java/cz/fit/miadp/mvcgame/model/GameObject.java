package cz.fit.miadp.mvcgame.model;

import cz.fit.miadp.mvcgame.config.GameConfig;
import cz.fit.miadp.mvcgame.visitor.IVisitor;

public abstract class GameObject
{
    private int posX = 100;
    private int posY = 100;

    public int getX() {
        return this.posX;
    }

    public int getY() {
        return this.posY;
    }

    public void setX(int x) {
        this.posX = x;
    }

    public void setY(int y) {
        this.posY = y;
    }

    public void move()
    {

    }

    public boolean collidesWith(GameObject otherGO)
    {
        boolean bResult = true;

        int aX = this.getX();
        int aY = this.getY();
        int bX = otherGO.getX();
        int bY = otherGO.getY();

        bResult = bResult && ((Math.abs(aX-bX) ) < GameConfig.COLLIDE_FACTOR);
        bResult = bResult && ((Math.abs(aY-bY) ) < GameConfig.COLLIDE_FACTOR);

        return bResult;
    }

    public abstract void acceptVisitor(IVisitor visitor);
}