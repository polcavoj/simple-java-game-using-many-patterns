package cz.fit.miadp.mvcgame.model;

import cz.fit.miadp.mvcgame.visitor.IVisitor;

public class Explosion extends TimelifeAwareGameObject
{
    
    public void acceptVisitor(IVisitor visitor){
        visitor.visitExplosion(this);
    }
}

