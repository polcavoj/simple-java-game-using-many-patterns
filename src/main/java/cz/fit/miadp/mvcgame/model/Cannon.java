package cz.fit.miadp.mvcgame.model;

import java.util.ArrayList;
import java.util.List;

import cz.fit.miadp.mvcgame.abstractFactory.IGameObjectsFactory;
import cz.fit.miadp.mvcgame.config.GameConfig;
import cz.fit.miadp.mvcgame.state.DoubleShootingMode;
import cz.fit.miadp.mvcgame.state.IShootingMode;
import cz.fit.miadp.mvcgame.state.SingleShootingMode;
import cz.fit.miadp.mvcgame.visitor.IVisitor;

/**
 *
 * @author Ondrej Stuchlik
 */
public class Cannon extends GameObject{
    protected static final IShootingMode singleShootingMode = new SingleShootingMode();
    protected static final IShootingMode doubleShootingMode = new DoubleShootingMode();

    protected IGameObjectsFactory goFact;
    protected Float velocity = 10.0f;
    protected Float angle = 0.0f;
    protected IShootingMode shootingMode = singleShootingMode;
    protected List<Missile> shootBatch;

    public Cannon(IGameObjectsFactory goFact){
        this.goFact = goFact;
        this.setX(50);
    }

    public Float getVelocity() {
        return velocity;
    }

    public Float getAngle() {
        return angle;
    }
    
    public void setVelocity(Float velocity) {
            this.velocity = velocity;
    }

    public void setAngle(Float angle) {
            this.angle = angle;
    }

    public List<Missile> shoot() {
            this.shootBatch = new ArrayList<Missile>();

            this.shootingMode.shoot(this);

            return this.shootBatch;
    }

    public void primitiveShoot()
    {
            this.shootBatch.add( this.goFact.createMissile() );
    }

    public void toggleShootingMode()
    {
            this.shootingMode.nextMode(this);
    }

    public void setDoubleShootingMode() {
            shootingMode = doubleShootingMode;
    }

    public void setSingleShootingMode() {
            shootingMode = singleShootingMode;
    }

    public void aimUp() {
        this.setAngle( this.getAngle() - GameConfig.ANGLE_STEP );
    }

    public void aimDown() {
        this.setAngle( this.getAngle() + GameConfig.ANGLE_STEP );
    }

    public void incPower() {
        this.setVelocity( this.getVelocity() + GameConfig.VELOCITY_STEP );
    }

    public void decPower() {
        this.setVelocity( this.getVelocity() - GameConfig.VELOCITY_STEP );
    }

    public void acceptVisitor(IVisitor visitor)
    {
        visitor.visitCannon(this);
    }
}
