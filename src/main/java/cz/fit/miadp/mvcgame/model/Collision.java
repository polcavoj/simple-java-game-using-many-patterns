package cz.fit.miadp.mvcgame.model;

import cz.fit.miadp.mvcgame.visitor.IVisitor;

public class Collision extends TimelifeAwareGameObject
{
    
    public void acceptVisitor(IVisitor visitor){
        visitor.visitCollision(this);
    }
}

