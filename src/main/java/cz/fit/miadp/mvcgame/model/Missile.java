package cz.fit.miadp.mvcgame.model;

import cz.fit.miadp.mvcgame.strategy.IMovementStrategy;
import cz.fit.miadp.mvcgame.visitor.IVisitor;

public class Missile extends TimelifeAwareGameObject
{
    protected int initX;
    protected int initY;
    protected Float initVelocity;
    protected Float initAngle;
    protected IMovementStrategy mStrategy;

    public Missile(int x, int y, Float velocity, Float angle, IMovementStrategy strategy)
    {
        initAngle = angle;
        initVelocity = velocity / 20;
        initX = x;
        initY = y;
        mStrategy = strategy;
        this.setX(initX);
        this.setY(initY);
    }
    
    public void move()
    {
        long lifetime = this.getLifetime();
        //int newX = (int)(initX + (initVelocity * lifetime * Math.cos(initAngle) ));
        //int newY = (int)(initY + (initVelocity * lifetime * Math.sin(initAngle) ));
        int newX = mStrategy.nextPosX(initX, initVelocity, initAngle, lifetime);
        int newY = mStrategy.nextPosY(initY, initVelocity, initAngle, lifetime);
        this.setX(newX);
        this.setY(newY);
    }

    public void acceptVisitor(IVisitor visitor)
    {
        visitor.visitMissile(this);
    }


}
