package cz.fit.miadp.mvcgame.model;

import cz.fit.miadp.mvcgame.visitor.IVisitor;

public class ModelInfo extends GameObject
{
    private GameModel model;

    public ModelInfo(GameModel model)
    {
        this.model = model;
        this.setX(10);
        this.setY(10);
    }


    public String getText()
    {
        return "Score: "+this.model.getScore()
                + " Cannon.y: "+this.model.getCannon().getY()
                + " Angle: "+this.model.getCannon().getAngle()
                + " Power: "+this.model.getCannon().getVelocity()
                + " Enem.size: "+(this.model.getEnemies().size() + this.model.getStrongerEnemies().size())
                + " Miss.size: "+this.model.getMissiles().size()
                ;
    }

    public void acceptVisitor(IVisitor visitor)
    {
        visitor.visitModelInfo(this);
    }
    

}
