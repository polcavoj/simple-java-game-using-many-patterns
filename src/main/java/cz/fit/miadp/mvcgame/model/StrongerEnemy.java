package cz.fit.miadp.mvcgame.model;

import cz.fit.miadp.mvcgame.config.GameConfig;
import cz.fit.miadp.mvcgame.visitor.IVisitor;

public class StrongerEnemy extends GameObject{
    
    public int health;
    protected int initX;
    protected int initY;
    /*
        Direction true - moving up,
        Direction false - moving down.
    */
    protected boolean direction;
    
    public StrongerEnemy(int x, int y){
        initX = x;
        initY = y;
        direction = false;
        this.setX(initX);
        this.setY(initY);
    }
    
    public void move(){
        if( this.getY() > 680 || (this.getY() - initY ) > GameConfig.MAX_RANGE_OF_ENEMY )
            direction = true;
        else if( this.getY() < 50 || (initY - this.getY()) > GameConfig.MAX_RANGE_OF_ENEMY )
            direction = false;
        
        int newY;
        if( direction ){
            newY = this.getY() - GameConfig.ENEMY_STEP;
        } else {
            newY = this.getY() + GameConfig.ENEMY_STEP;
        }
        
        this.setY(newY);
    }
    
    public void acceptVisitor(IVisitor visitor){
        visitor.visitStrongerEnemy(this);
    }
    
    public void setHealth(int health){
        this.health = health;
    }
    
    public int getHealth(){
        return this.health;
    }

}
