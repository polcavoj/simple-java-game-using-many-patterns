package cz.fit.miadp.mvcgame.visitor;

import cz.fit.miadp.mvcgame.model.Enemy;
import cz.fit.miadp.mvcgame.model.Cannon;
import cz.fit.miadp.mvcgame.model.ModelInfo;
import cz.fit.miadp.mvcgame.model.Collision;
import cz.fit.miadp.mvcgame.model.Explosion;
import cz.fit.miadp.mvcgame.model.Missile;
import cz.fit.miadp.mvcgame.model.StrongerEnemy;

public interface IVisitor
{
    void visitCannon(Cannon cannon);
    void visitEnemy(Enemy enemy);
    void visitStrongerEnemy(StrongerEnemy strongerEnemy);
    void visitCollision(Collision collision);
    void visitExplosion(Explosion explosion);
    void visitMissile(Missile missile);
    void visitModelInfo(ModelInfo modelInfo);
}