# Semestrální práce
* MVC hra
* výběr z vlastností
  * obsahuje vzory: MVC, Strategy, Proxy, State, Visitor, Observer, Command, Memento, Abstract factory
  * +změna síly střely (force)
  * +úhlu kanónu (angle)
  * gravitace (gravity)
  * +počítání score
  * +možnost vystřelit více střel najednou (State)
  * +ovládání modelu pomocí commandů (vzor Command)
  * +krok-zpět (Memento&Command)
  * +2 strategie pohybu střely (Strategy)
* v projektu bude alespoň 5 unit testů (test cases) v alespoň 2 test suitech a bude použito mockování

## Controls
* **Arrow up** - Move cannon up
* **Arrow down** - Move cannon down
* **Space** - Cannon shoot
* **W** - Aim cannon up
* **S** - Aim cannon down
* **E** - Increase cannon power
* **Q** - Decrease cannon power
* **C** - Switch movement strategy - balistic and straight strategy.
* **X** - Toogle shooting mode - One shot and double shot mode.
* **Z** - Undo last command.